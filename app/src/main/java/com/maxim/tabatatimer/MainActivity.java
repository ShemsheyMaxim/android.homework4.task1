package com.maxim.tabatatimer;


import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "myLogs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frame_layout);

        Button buttonTimer = (Button) findViewById(R.id.button_select_fragment_timer);
        Button buttonStopwatch = (Button) findViewById(R.id.button_select_fragment_stopwatch);

        buttonTimer.setOnClickListener(this);
        buttonStopwatch.setOnClickListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
//        outState.putString(EDIT_TEXT_KEY, editText.getText());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onClick(View view) {
        FragmentTimer fragmentTimer = new FragmentTimer();
        FragmentStopwatch fragmentStopwatch = new FragmentStopwatch();
        android.app.FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        switch (view.getId()) {
            case (R.id.button_select_fragment_timer):
                fragmentTransaction.replace(R.id.frame_layout, fragmentTimer);
                break;
            case (R.id.button_select_fragment_stopwatch):
                fragmentTransaction.replace(R.id.frame_layout, fragmentStopwatch);
                break;
        }
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
